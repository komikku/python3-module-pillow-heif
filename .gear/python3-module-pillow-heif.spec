%define _unpackaged_files_terminate_build 1
%define pypi_name pillow-heif
%define mod_name pillow_heif

%def_with check

Name: python3-module-%pypi_name
Version: 0.17.0
Release: alt1
Summary: Python implementation of Protocol Buffers with dataclass-based schemas
License: BSD-3-Clause
Group: Development/Python3
Url: https://pypi.org/project/pillow-heif/
Vcs: https://github.com/bigcat88/pillow_heif.git
BuildArch: x86_64

Source0: %name-%version.tar
Source1: %pyproject_deps_config_name

%pyproject_runtimedeps_metadata
BuildRequires(pre): rpm-build-pyproject
BuildRequires: libheif-devel
%pyproject_builddeps_build

%if_with check
%pyproject_builddeps_metadata_extra tests
BuildRequires: python3-module-numpy-testing
%endif

%description
The pillow-heif library provides Python bindings to libheif, enabling
efficient handling of HEIF (High Efficiency Image Format) images and
integrating seamlessly with the Pillow library as a plugin. This
comprehensive tool allows developers to decode and encode 8, 10, and
12-bit HEIC and AVIF files, offering extensive support for EXIF, XMP,
and IPTC metadata operations. Additionally, it supports multiple images
within a single file, the PrimaryImage attribute, and adding or removing
thumbnails.

Key features include:
- Decoding and encoding of HEIC and AVIF files with 8, 10, and 12-bit
  depth.
- Comprehensive metadata support including EXIF, XMP, and IPTC.
- Handling multiple images in one file and managing PrimaryImage
  attributes.
- Adding and removing thumbnails.
- Reading depth images.
- Simple integration with Pillow through a single line of code.

%prep
%setup
%pyproject_deps_resync_build
%pyproject_deps_resync_metadata

%build
%pyproject_build

%install
%pyproject_install

%check
%pyproject_run_pytest -ra

%files
%doc LICENSE* README.md CHANGELOG.md
%python3_sitelibdir/%mod_name
%python3_sitelibdir/_%mod_name.*
%python3_sitelibdir/%{pyproject_distinfo %mod_name}

%changelog
* Thu Jun 27 2024 Aleksandr A. Voyt <sobue@altlinux.org> 0.17.0-alt1
- Initial commit
